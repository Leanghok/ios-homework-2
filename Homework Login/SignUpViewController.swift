//
//  SignUpViewController.swift
//  Homework Login
//
//  Created by Hour Leanghok on 11/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    var signUpDelegate: DidSignUpDelegate?
    
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var inputUsernameLabel: UILabel!
    
    
    @IBOutlet weak var inputNewPasswordLabel: UILabel!
    
    @IBOutlet weak var inputConfirmPasswordLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inputUsernameLabel.text = ""
        inputNewPasswordLabel.text = ""
        inputConfirmPasswordLabel.text = ""
        // Do any additional setup after loading the view.
    }
    
    @IBAction func doneButtonTap(_ sender: UIButton) {
        
        guard usernameTextField.text != "" else {
            inputUsernameLabel.text = "Please input username"
            return
        }
        inputUsernameLabel.text = ""
        
        guard newPasswordTextField.text != "" else {
            inputNewPasswordLabel.text = "Please input new password"
            return
        }
        inputNewPasswordLabel.text = ""
        
        guard confirmPasswordTextField.text != "" else {
            inputConfirmPasswordLabel.text = "Please confirm your password"
            return
        }
        inputConfirmPasswordLabel.text = ""
        
        guard newPasswordTextField.text == confirmPasswordTextField.text else {
            inputConfirmPasswordLabel.text = "Password does not match"
            return
        }
        inputConfirmPasswordLabel.text = ""
        
        signUpDelegate?.didSignUpDelegate(username: usernameTextField.text!, password: confirmPasswordTextField.text!)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancelButtonTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
