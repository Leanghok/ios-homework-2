//
//  SignUpDelegate.swift
//  Homework Login
//
//  Created by Hour Leanghok on 11/12/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation

protocol DidSignUpDelegate {
    func didSignUpDelegate(username:String, password:String)
}
