//
//  LoginViewController.swift
//  Homework Login
//
//  Created by Hour Leanghok on 11/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var inputUsernameLabel: UILabel!
    
    @IBOutlet weak var inputPasswordLabel: UILabel!
    
    var didFinishLogin: DidFinishLoginDelegate?
    var users:[User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users.append(User(username: "admin",password: "admin"))
        inputUsernameLabel.text = ""
        inputPasswordLabel.text = ""
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func loginButtonTap(_ sender: UIButton) {
        
        guard usernameTextField.text != "" else {
            inputUsernameLabel.text = "Please input username"
            return
        }
        inputUsernameLabel.text = ""
        
        guard passwordTextField.text != "" else {
            inputPasswordLabel.text = "Please input password"
            return
        }
        inputPasswordLabel.text = ""
        
        for user in users{
            if(user.username == usernameTextField.text && user.password == passwordTextField.text){
                didFinishLogin?.didFinishLoginDelegate(username: usernameTextField.text!)
                dismiss(animated: true, completion: nil)
                break
            }else{
                inputPasswordLabel.text = "Username or password does not match"
            }
        }
    }
    
    @IBAction func signUpButtonTap(_ sender: UIButton) {
        let signUpViewBoard = storyboard?.instantiateViewController(withIdentifier: "SignUpView") as! SignUpViewController
        signUpViewBoard.signUpDelegate = self
        present(signUpViewBoard, animated: true, completion: nil)
        
    }
    
}
extension LoginViewController: DidSignUpDelegate{
    func didSignUpDelegate(username: String, password: String) {
        users.append(User(username: username, password: password))
    }
}
