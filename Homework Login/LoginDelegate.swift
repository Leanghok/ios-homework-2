//
//  LoginDelegate.swift
//  Homework Login
//
//  Created by Hour Leanghok on 11/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation

protocol DidFinishLoginDelegate {
    func didFinishLoginDelegate (username: String)
}

protocol WillLoginDelegate {
    func willLoginDelegate(username: String, password: String)
}
