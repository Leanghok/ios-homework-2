//
//  ViewController.swift
//  Homework Login
//
//  Created by Hour Leanghok on 11/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func loginButtonTap(_ sender: Any) {
        
        if let buttonTitle = loginButton.titleLabel?.text {
            if buttonTitle == "Login"{
                let loginView = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LoginViewController
                loginView.didFinishLogin = self
                present(loginView, animated: true, completion: nil)
            }else{
                usernameLabel.text = ""
                loginButton.setTitle("Login", for: .normal)
            }
        }
        
        
    }
}
extension ViewController : DidFinishLoginDelegate{
    func didFinishLoginDelegate(username: String) {
        usernameLabel.text = "Hey " + username + "!"
        loginButton.setTitle("Logout", for: .normal)
    }
}

