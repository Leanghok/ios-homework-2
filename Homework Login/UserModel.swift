//
//  UserModel.swift
//  Homework Login
//
//  Created by Hour Leanghok on 11/11/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation

class User{
    var username: String
    var password: String
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
}
