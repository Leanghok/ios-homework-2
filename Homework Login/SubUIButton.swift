//
//  SubUIButton.swift
//  Homework Login
//
//  Created by Hour Leanghok on 11/12/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    open override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 20
        self.layer.masksToBounds = true
    }
}
